import jumbo
import logging
import argparse
import datetime

import sql  # src/sql
import cli  # src/cli
import utils  # src/utils


from pathlib import Path


if __name__ == "__main__":

    # Setup a basic logger
    logging.basicConfig(
        format='[%(asctime)s] %(levelname)s | %(message)s',
        datefmt='%D %H:%M:%S'
    )

    logger = logging.getLogger(__name__)
    logger.setLevel("INFO")

    print('\n')
    logger.info("Welcome to the BBR Data Downloader! \U0001F680 \n")
    logger.info("commit SHA: \t%s\n", cli.get_git_revision_hash())

    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-plw", help="reference .PLW file",
                        required=True, type=str)
    parser.add_argument("-t", help=".PLW data acquisition period (s)",
                        required=False, type=float, default=1)
    parser.add_argument("-dir", help="output files directory",
                        required=False, type=str, default=None)
    args = parser.parse_args()

    # Check validity of input .PLW file path
    cli.check_plw(args.plw)
    plw_file = Path(args.plw)  # materialise

    # Check data sampling period is valid
    cli.check_plw_period(args.t)
    plw_period = args.t  # materialise

    # Create default folder for output BBR logs
    save_dir = Path(args.dir) if args.dir is not None else Path(
        __file__).parent / 'BBR Shifts'

    if not save_dir.is_dir():
        save_dir.mkdir(parents=True, exist_ok=True)

    # load the config file with all pt104 parameters
    yaml_params = cli.load_yaml_config()
    clocks = yaml_params.clocks

    # Log out relevant CLI parameters values
    logger.info("Input: \t %s", plw_file)
    logger.info("Output:\t %s\n", save_dir)

    # MAIN

    # Get start and end timestamps for dataset
    logger.info('Scanning .PLW file...')
    start_timestamp, end_timestamp = utils.get_plw_times(plw_file,
                                                         period=plw_period)
    logger.info("\tDONE")
    
    # Initialize database connection to download relevant BBR corrections
    # jumbo.env file should be in root folder of this project
    logger.info("Starting database connection...")
    database = jumbo.database.Database("../")

    # Open a connection pool.
    with database.open() as pool:
        # Get an individual connection from the pool.
        with pool.connect(key=1):
            logger.info("\tDONE")

            # Pull data from database;
            logger.info("Downloading BBR data from database...")
            results = pool.send(sql.SELECT_TIMERANGE_FROM_BBR_CORRECTIONS,
                                subs=(start_timestamp, end_timestamp), key=1)
            logger.info("\tDONE")

            # Convert results to a pandas DataFrame
            logger.info("Converting to dataframe...")
            bbr_df = jumbo.utils.convert_to_df(results)
            logger.info("\tDONE")

            # Split into individual clocks bbr data and save to file
            logger.info("Writing data to file...")
            for clock in clocks:
                logger.info("\t%s data", clock)

                df_clock = bbr_df.loc[bbr_df['clock'] == clock]

                fn = save_dir / f"{plw_file.stem}_BBR_{clock}.txt"

                # Write metadata to file
                with open(fn, 'w') as f:
                    f.write(f'# BBR METADATA:\n'
                            f'# FETCHED: \t{datetime.datetime.now()}\n'
                            f'# SERVER:  \t{pool.config.DATABASE_HOST}\n'
                            f'# DATABASE:\t{pool.config.DATABASE_NAME}\n'
                            f'# SCHEMA:  \tstrontium_lattice\n'
                            f'# TABLE:   \tbbr_corrections\n'
                            f'# BY:      \t{pool.config.DATABASE_USERNAME}\n'
                            f'# REF:     \t{plw_file}\n'
                            f'# \n')

                # Write data to file
                df_clock.to_csv(fn, sep='\t', mode='a')

            logger.info('\tDONE')

    logger.info("ALL DONE! \U0001F44D")

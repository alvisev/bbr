import os
import logging
import pycotech
import numpy as np
import pandas as pd
import cli  # src/cli
from datetime import datetime
from pathlib import Path
from typing import Union, List, overload, NamedTuple


TimeTuple = NamedTuple('TimeTuple', [('start', float), ('stop', float)])

# Instantiate module-level logger
logger = logging.getLogger(__name__)
logger.setLevel("INFO")


@overload
def get_calibration(temperature: None,
                    calibrations: cli.Calibrations,
                    label: str) -> None: ...


@overload
def get_calibration(temperature: float,
                    calibrations: cli.Calibrations,
                    label: str) -> float: ...


def get_calibration(temperature, calibrations, channel_label):
    """Returns the calibration correction to apply to the temperature reading
    for a given PicoLogger PT-104 channel. The correction is estimated using
    linear interpolation between calibration corrections at known temperatures.

    Args:
        temperature:    uncalibrated temperature reading, in Celsius.
        calibrations:   dictionary with calibrations for PicoLogger PT-104
                        channels. Format:
                        {channel_label: {Temperature *C: correction, ...}, ...}
        channel_label:  software name of PicoLogger PT-104 channel from which
                        the reading has been taken from.

    Returns:
        calibrated correction to apply to the temperature reading.
    """

    # Skip if None reading
    if temperature:

        # Create lists of calibration temperatures and associated corrections
        # for given channel
        calibration_temperatures = list(calibrations[channel_label].keys())
        corrections = list(calibrations[channel_label].values())

        return np.interp(temperature, calibration_temperatures, corrections)

    else:
        print(f"Cannot retrieve interpolated calibrated correction for "
              f"temperature reading of {temperature} *C")
        return None


def calc_shift(temperature):
    """Calculates the Black Body Radiation (BBR) shift (static + dynamic) for
    a given temperature.

    Args:
        temperature (float): temperature, in Kelvin.

    Returns:
        (float) black body radiation shift for black body of given temperature.
    """

    # Define Physical Constants TODO: link source
    static_shift_coeff = -2.13023
    dyn_shift_coeff_6 = -0.1291
    dyn_shift_coeff_8 = -0.01616
    dyn_shift_coeff_10 = -0.00343
    t_ref = 300  # reference temperature for BBR corrections, in Kelvins

    normalized_temperature = temperature/t_ref

    return static_shift_coeff * normalized_temperature ** 4 + dyn_shift_coeff_6 * normalized_temperature ** 6 +\
           dyn_shift_coeff_8 * normalized_temperature ** 8 + dyn_shift_coeff_10 * normalized_temperature ** 10


def get_bbr_shift(mean_temperature, temperature_gradient):
    """ Calculates the Black Body Radiation (BBR) shift (static + dynamic) with uncertainties.

    Args:
        mean_temperature (float): mean black body temperature, in degrees C
        temperature_gradient: maximum temperature gradient across the black body, in degrees C

    Returns:
        (tuple of floats) black body radiation shift, lower uncertainty limit, upper uncertainty limit
    """

    odeg = 273.15  # 0 degrees *C in Kelvins

    # Convert temperatures to Kelvins and calculate upper and lower temperature bounds
    mean_temperature_norm = (mean_temperature + odeg)
    mean_temperature_norm_lo = (mean_temperature + odeg - temperature_gradient/np.sqrt(12))
    mean_temperature_norm_up = (mean_temperature + odeg + temperature_gradient/np.sqrt(12))

    # Calculate BBR shifts at those temperatures
    bbr_shift = calc_shift(mean_temperature_norm)
    bbr_shift_lo = calc_shift(mean_temperature_norm_lo)
    bbr_shift_up = calc_shift(mean_temperature_norm_up)

    return bbr_shift, bbr_shift_lo, bbr_shift_up


def get_freq_shift(mean_temperature, temperature_gradient, clock_frequency=429228004229873.0):
    """ Calculates frequency shift due to Black Body Radiation shift

    Args:
        mean_temperature (float): mean black body temperature, in degrees C
        temperature_gradient: maximum temperature gradient across the black body, in degrees C
        clock_frequency: unperturbed clock transition frequency, in Hz (see Refs)

    Returns:
        (tuple of floats) BBR frequency shift and uncertainty, in Hz

    Refs:
        The default clock frequency used here is the BIPM Sr87 recommended value found here:
        https://www.bipm.org/utils/common/pdf/mep/87Sr_429THz_2018.pdf

    """

    bbr_shift, bbr_shift_lo, bbr_shift_up = get_bbr_shift(mean_temperature, temperature_gradient)

    return bbr_shift/clock_frequency, (bbr_shift_up - bbr_shift_lo)/(2*clock_frequency)


def pack_pico_stream(df: pd.DataFrame, devices: cli.Devices,
                     calibrations: cli.Calibrations,
                     end_timestamp: float,
                     period: float) -> pd.DataFrame:
    """Packs and processes raw data from a PicoLogger data-stream and
    generates a dataframe ready to be uploaded onto the PostgreSQL database.

    The input data is timestamped (unix and iso), tagged with channel
    and acquisition device identifiers, and calibrated by interpolation of
    the provided calibration values.

    Args:
        df:             input PicoLogger data-stream. The dataframe should
                        have a `channel` column holding channel labels (str),
                        and a `temp` column holding associated temperature
                        readings (float).
        devices:        dictionary mapping acquisition channel label stumps (
                        X*) to the id of the device they are associated to.
        calibrations:   dictionary mapping acquisition channel labels to a
                        mapping between reference temperature values and
                        associated calibration corrections.
        end_timestamp:  timestamp of the last sample in the input data-stream.
        period:         data sampling period, in seconds. This should be the
                        interval between each individual entries in the stream.

    Returns:
        dataframe formatted to match the PostgreSQL database table it should be
        saved to.

        index:      None (enumeration of entries)
        columns:    `iso_timestamp`, `unix_timestamp`, `device`, `channel`,
                    `label`, `calibrated_temperature`, `calibration_applied`
    """

    # Calculate UNIX timestamp for each entry
    logger.info('\ttimestamping entries (unix)')
    df['unix_timestamp'] = end_timestamp - (df.index.values[::-1] * period)

    # Add columns to match PostgreSQL bbr_logs table
    logger.info('\ttimestamping entries (iso)')
    df['iso_timestamp'] = df['unix_timestamp'].apply(
        lambda x: datetime.fromtimestamp(x).isoformat())

    logger.info('\ttagging picologger device id')
    df['device'] = df['channel'].apply(lambda x: devices.get(x[0] + '*'))

    logger.info('\ttagging acquisition channel number')
    df['num'] = df['channel'].apply(lambda x: int(x[1]))

    logger.info('\tapplying calibrations')
    df['calibration_applied'] = df[['temp', 'channel']].apply(
        lambda x: get_calibration(x[0], calibrations=calibrations,
                                  channel_label=x[1]), axis=1)

    df['calibrated_temperature'] = df['temp'] + df['calibration_applied']
    logger.info('\tDONE')

    # Format dataframe with same columns as bbr_logs table
    logger.info('Formatting dataframe...')
    df['label'], df['channel'] = df['channel'], df['num']
    df = df[['iso_timestamp', 'unix_timestamp', 'device', 'channel',
             'label', 'calibrated_temperature', 'calibration_applied']]

    return df


def build_bbr_corrections(df: pd.DataFrame,
                          clock_channels: cli.Channels,
                          excluded_channels: cli.Channels,
                          win_size: int) -> pd.DataFrame:
    """Builds a dataframe with calculated black-body radiation corrections
    from an input data-stream of timestamped calibrated temperatures. This
    data stream can consist of data acquired from separate systems. The
    output dataframe is ready to be uploaded onto the PostgreSQL database.

    The input dataframe is expected to have the same structure as the
    PostgreSQL database table holding the calibrated temperature measurements.
    This can be achieved by `SELECT`ing data directly from the database,
    or building the data locally via utils.pack_pico_stream().

    Args:
        df:                 input PicoLogger data-stream, database-formatted.
                            Should be a timestamped stream of temperature
                            measurements and associated metadata. The dataframe
                            should have `iso_timestamp` (timestamp)
                            and `unix_timestamp` (float) timestamp columns,
                            a `label` (str) column holding channel labels,
                            and a `calibrated_temperature` (float) column holding
                            associated calibrated temperature readings.
        clock_channels:     mapping between clock labels and associated data
                            acquisition channels.
        excluded_channels:  mapping between clock labels and associated data
                            acquisition channels which should not be included
                            in the BBR calculation.
        win_size:           size of the rolling average window used to smooth
                            the data-stream across each channel, in units of the
                            single channel acquisition period (usually 3s).

    Returns:
        dataframe formatted to match the PostgreSQL database table it should be
        saved to.

        index:      None (enumeration of entries)
        columns:    `iso_timestamp`, `unix_timestamp`, `clock`, `channel`,
                    `max_temp`, `min_temp`, `max_grad`, `shift`, `shift_err`
    """

    # initialise dataframe to hold bbr corrections
    df_bbr = df[['unix_timestamp', 'iso_timestamp', 'label',
                 'calibrated_temperature']].copy()

    bbr_dfs = []
    for clk, labels in clock_channels.items():

        # Select all channels for this clock
        df_temp = df_bbr[df_bbr['label'].isin(labels)].copy()
        df_temp.reset_index(drop=True, inplace=True)

        # select labels which should not be used for bbr
        excluded = excluded_channels.get(clk, [])

        logger.debug(f"\tcalculating {clk} frequency corrections")
        # Calculate bbr shift and corrections for given clock data
        df_temp = calc_bbr_corrections(df_temp, clk_label=clk,
                                       excl_labels=excluded,
                                       win_size=win_size)

        bbr_dfs.append(df_temp)

    logger.debug("Assembling...")
    df_bbr = pd.concat(bbr_dfs, ignore_index=True).sort_values(
        'unix_timestamp').reset_index(drop=True)

    return df_bbr


def calc_bbr_corrections(df: pd.DataFrame,
                         clk_label: str,
                         excl_labels: List[str],
                         win_size: int) -> pd.DataFrame:
    """Builds a dataframe with calculated black-body radiation corrections
    from an input data-stream of timestamped calibrated temperatures. This
    data stream should consist of data associated with a single system. The
    output dataframe is ready to be uploaded onto the PostgreSQL database.

    The input dataframe is expected to have the same structure as the
    PostgreSQL database table holding the calibrated temperature measurements.
    This can be achieved by `SELECT`ing data directly from the database,
    or building the data locally via utils.pack_pico_stream().

    Args:
        df:             input PicoLogger data-stream, database-formatted.
                        Should be a timestamped stream of temperature
                        measurements and associated metadata. The dataframe
                        should have `unix_timestamp` (float) and
                        `iso_timestamp` (timestamp) timestamp columns,
                        a `label` (str) column holding channel labels,
                        and a `calibrated_temperature` (float) column holding
                        associated calibrated temperature readings.
        clk_label:      label of the system whose temperatures are being
                        sampled.
        excl_labels:    labels of temperature sensors which should not be
                        used for BBR correction calculation.
        win_size:       size of the rolling average window used to smooth
                        the data-stream across each channel, in units of the
                        single channel acquisition period (usually 3s).

    Returns:
        dataframe formatted to match the PostgreSQL database table it should be
        saved to.

        index:      None (enumeration of entries)
        columns:    `iso_timestamp`, `unix_timestamp`, `clock`, `channel`,
                    `max_temp`, `min_temp`, `max_grad`, `shift`, `shift_err`
    """

    # Assign clock id
    df['clock'] = clk_label

    # List all associated channel labels
    labels = df['label'].unique()

    # Calculate rolling average for each individual channel readings
    for label in labels:

        df[f'rolling {label} temp'] = \
            df.loc[df['label'] == label,
                   'calibrated_temperature'].rolling(win_size).mean()

        # Forward propagate values during measurements of other channels
        df[f'rolling {label} temp'].fillna(method='ffill', inplace=True)

    rolling_columns = [name for name in list(df.columns) if
                       name.startswith('rolling')]

    # exclude columns for sensors not monitoring clock chamber
    rolling_columns_excluded = [c for c in rolling_columns for n in
                                excl_labels if n in c]
    rolling_columns = [n for n in rolling_columns if n not in
                       rolling_columns_excluded]
    
    # Calculate max gradients across all channels at each instant in time
    df['max_temp'] = df[rolling_columns].max(axis=1)
    df['min_temp'] = df[rolling_columns].min(axis=1)
    df['max_grad'] = df['max_temp'] - df['min_temp']
    df['mean'] = (df['max_temp'] + df['min_temp']) / 2

    # Exclude gradient calculated across rows where there were incomplete
    # sets of rolling means (i.e. some channels missing)
    df.loc[df[rolling_columns].isna().any(axis=1),
           ['max_temp', 'min_temp', 'max_grad', 'mean']] = np.NaN

    # Calculate frequency shift and uncertainty due to BBR on the clock
    # transition at any given time
    shift, shift_err = get_freq_shift(df['mean'], df['max_grad'])
    df['shift'], df['shift_err'] = shift, shift_err

    # Backfill starting values excluded from the rolling window
    df.fillna(method='bfill', inplace=True)

    # Format dataframe with same columns as bbr_corrections table
    df['channel'] = df['label']
    df = df[['iso_timestamp', 'unix_timestamp', 'clock', 'channel',
             'max_temp', 'min_temp', 'max_grad', 'shift', 'shift_err']].copy()

    return df


def get_plw_times(fn: Union[str, Path], period: float) -> TimeTuple:
    """Returns the UNIX time interval over which data in a .PLW file was
    acquired.

    .. caution::

        This function assumes that data was taken uninterruptedly.

    Args:
        fn:         path to the .PLW file
        period:     data sampling period, in seconds. This should be time
                    interval between .PLW rows.

    Returns:
        tuple of UNIX start and end times corresponding to the time interval
        over which the .PLW data was acquired. Start and end times are the
        timetags of the first datapoint of the first row, and of the last
        datapoint of the last row.
    """

    # Read in file to count number of channels and entries
    df = pycotech.utils.read_plw(fn=fn)

    # Calculate time interval between each stream sample
    stream_period = period / len(df.columns)

    # Time the .PLW file was created (end of recording). This is the
    # start time of the next file created by PLW player - so assume the last
    # datapoint recorded in the file is 1 fractional period before this time.
    ctime = os.path.getctime(fn) - stream_period

    # Time of last datapoint of first entry
    stime = ctime - (len(df.index) - 1) * period

    # Time of first datapoint of first entry
    stime = stime - period + stream_period

    return TimeTuple(start=stime, stop=ctime)


def to_iso_timestamp(unix_timestamp: float) -> str:
    """Convert an unix timestamp to an ISO formatted timestamp"""

    return datetime.fromtimestamp(unix_timestamp).isoformat()

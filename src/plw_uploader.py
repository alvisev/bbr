"""Description here
"""

__author__ = "Alvise Vianello"
__status__ = "Prototype"
__version__ = "1.0.0"

import jumbo
import logging
import argparse
import pycotech
import sql  # src/sql
import cli  # src/cli
from pathlib import Path

import utils

if __name__ == "__main__":

    # Setup a basic logger
    logging.basicConfig(
        format='[%(asctime)s] %(levelname)s | %(message)s',
        datefmt='%D %H:%M:%S'
    )

    logger = logging.getLogger(__name__)
    logger.setLevel("DEBUG")

    print('\n')
    logger.info("Welcome to the BBR Data Builder! \U0001F680 \n")
    logger.info("commit SHA: \t%s\n", cli.get_git_revision_hash())

    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-plw", help="input .PLW file",
                        required=True, type=str)
    parser.add_argument("-t", help=".PLW data acquisition period (s)",
                        required=False, type=float, default=1)
    parser.add_argument("-y", help="PostgreSQL role has ownership",
                        required=False, action='store_true')
    parser.add_argument("--no_upload", help="disable database interactions ",
                        required=False, action='store_true')

    args = parser.parse_args()

    # Check validity of input .PLW file path
    cli.check_plw(args.plw)
    plw_file = Path(args.plw)  # materialise

    # Check data sampling period is valid
    cli.check_plw_period(args.t)
    plw_period = args.t  # materialise

    # Force user to confirm he has ownership on database
    if not args.no_upload and not args.y:
        cli.force_rerun(check_tables=['strontium_lattice.bbr_logs',
                                      'strontium_lattice.bbr_corrections'])

    # load the config file with all pt104 parameters
    yaml_params = cli.load_yaml_config()
    ch_to_device = yaml_params.devices
    ch_to_calibrations = yaml_params.calibrations
    clk_to_channels = yaml_params.channels
    clk_to_channels_excluded = yaml_params.channels_excluded

    # MAIN

    # RAW DATA DATAFRAME
    logger.info('Preparing data...')

    # Get start and end timestamps for dataset
    logger.info('\tparsing .PLW file...')
    start_timestamp, end_timestamp = utils.get_plw_times(plw_file,
                                                         period=plw_period)

    # Read .PLW file
    logger.info('\tconverting to data-frame...')
    df_picodata = pycotech.utils.read_plw(plw_file)

    # Convert to data-stream to match database bbr_logs table architecture
    logger.info("\tconverting to data-stream")
    df_picodata = pycotech.utils.to_pico_stream(df_picodata)
    logger.info('\tDONE')

    # Calculate time interval between each stream sample
    channels = df_picodata['channel'].unique()
    stream_period = plw_period / len(channels)

    # Apply calibrations and format like bbr_logs PostgreSQL table
    logger.info('Processing data...')
    df_picodata = utils.pack_pico_stream(df_picodata,
                                         devices=ch_to_device,
                                         calibrations=ch_to_calibrations,
                                         end_timestamp=end_timestamp,
                                         period=stream_period)
    logger.info('\tDONE')

    # ASSOCIATED BBR CORRECTIONS DATAFRAME

    logger.info("Processing systematics...")
    df_bbr = utils.build_bbr_corrections(
        df_picodata, clock_channels=clk_to_channels,
        excluded_channels=clk_to_channels_excluded, win_size=120)

    logger.info('\tDONE')

    # Convert to ISO to index tables
    start_timestamp, end_timestamp = utils.to_iso_timestamp(
        start_timestamp), utils.to_iso_timestamp(end_timestamp)

    # Upload results to database
    if not args.no_upload:

        logger.info("Starting database connection...")

        # Initialize database connection to upload temperature readings
        # jumbo.env file should be in root folder of this project
        database = jumbo.database.Database("../")

        # Open a connection pool.
        with database.open() as pool:
            logger.info("\tclient:   \t%s", pool.config.DATABASE_USERNAME)
            logger.info("\tserver:   \t%s", pool.config.DATABASE_HOST)
            logger.info("\tdatabase: \t%s", pool.config.DATABASE_NAME)
            logger.info("\tschema:   \tbbr")
            logger.info("\ttables:   \t[logs, corrections]")

            # Get an individual connection from the pool.
            with pool.connect(key=1):

                logger.info("\tDONE")

                # Switch to rwx role so that can work on schema
                pool.send(sql.SET_ROLE_RWX, key=1)

                logger.info('Creating table on database (if not present)...')
                # Initialize the table to hold temperature logs;
                pool.send(sql.CREATE_TABLE_BBR_LOGS, key=1)
                logger.info('Converting to hypertable (if not already '
                            'done)...')
                # Convert to hypertable
                pool.send(sql.CREATE_BBR_LOGS_HYPERTABLE, key=1)
                logger.info('Creating table on database (if not present)...')
                # Initialize the table to hold bbr analysis results;
                pool.send(sql.CREATE_TABLE_BBR_CORRECTIONS, key=1)
                logger.info('Converting to hypertable (if not already '
                            'done)...')
                # Convert to hypertable
                pool.send(sql.CREATE_BBR_CORRECTIONS_HYPERTABLE, key=1)
                logger.info("\tDONE")

                logger.info('Deleting possibly duplicate data from database...')
                pool.send(sql.DELETE_TIMERANGE_FROM_BBR_LOGS,
                          subs=(start_timestamp, end_timestamp), key=1)
                logger.info('\tDeleting associated bbr corrections')
                pool.send(sql.DELETE_TIMERANGE_FROM_BBR_CORRECTIONS,
                          subs=(start_timestamp, end_timestamp), key=1)
                logger.info('\tDONE')

                # Dump batch data on the database
                logger.info('Uploading data to PostgreSQL database...')
                # Takes 30s to upload a dataframe of 100000x20 samples

                logger.info('\tcalibrated temperature readings')
                pool.copy_df(df_picodata, db_table='logs',
                             schema='bbr', replace=False, key=1)

                logger.info('\tassociated bbr corrections')
                pool.copy_df(df_bbr, db_table='corrections',
                             schema='bbr', replace=False, key=1)

                logger.info('\tDONE')

    logger.info("ALL DONE! \U0001F44D")




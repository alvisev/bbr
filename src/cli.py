import sys
import yaml
import logging
import subprocess
from pathlib import Path
from typing import List, Dict, NamedTuple, Union, NoReturn

# spawn module-level logger
logger = logging.getLogger(__name__)
logger.setLevel("INFO")

# Type all Yaml Config parameters and build NamedTuple holding all of them
Calibrations = Dict[str, Dict[float, float]]
Channels = Dict[str, List[str]]
Clocks = List[str]
Devices = Dict[str, str]
Legend = Dict[str, str]
YamlParams = NamedTuple('YamlParams', [('calibrations', Calibrations),
                                       ('channels', Channels),
                                       ('channels_excluded', Channels),
                                       ('clocks', Clocks),
                                       ('devices', Devices),
                                       ('legend', Legend),
                                       ])


def load_yaml_config(fn: Union[str, Path] = 'config.yaml') -> YamlParams:
    """Loads experimental parameters from YAML configuration file.

    Example:

        .. code-block:: python

            yaml_params = cli.load_yaml_config()

            calibrations = yaml_params.calibrations

    Args:
        fn (optional):  path to YAML configuration file. Defaults to
                        ``config.yaml``.

    Returns:
        NamedTuple of experimental parameters loaded from YAML configuration
        file. Individual fields are accessed through the key they are
        referenced by in the YAML file.
    """

    try:

        # Load from YAML configuration file
        with open(fn, 'rb') as f:
            conf = yaml.load(f, Loader=yaml.SafeLoader)
            # ^ dict with a key for each top-level key in the YAML file

        # Unpack all yaml elements in corresponding NamedTuple fields
        return YamlParams(**conf)

    except FileNotFoundError as e:
        logger.warning('Could not locate the %s configuration file', fn)
        sys.exit()


def get_git_revision_hash(short: bool = True) -> str:
    """Returns the git commit SHA for the code being run.

    Args:
        short (optional):   if ``True`` truncates the commit SHA to the
                            first 8 characters only. Defaults to ``True``.

    Returns:
        git commit SHA.

    References:
        https://stackoverflow.com/a/21901260
    """

    try:
        sha = subprocess.check_output(['git', 'rev-parse', 'HEAD'])
    except subprocess.CalledProcessError:
        logger.exception(f"Git might not be installed on this machine")
        sys.exit()

    sha = sha.decode('ascii').strip()

    return sha[:8] if short else sha


def force_rerun(check_tables: List[str]) -> NoReturn:
    """Interrupts CLI program and encourages user to double check that there
    are no PostgreSQL triggers enabled on tables of interest, or that user has
    ownership to alter those triggers.

    Args:
        check_tables:   list of PostgreSQL database tables to be checked for.
    """

    if len(check_tables) == 0:
        raise ValueError("Specify which table(s) should be protected from "
                         "indiscriminate access")

    banner = "Make sure that there are no triggers enabled on the " \
             "following database relations, or that your PostgreSQL " \
             "user account has ownership to disable them."\
             "\n\nRelations:"

    for table in check_tables:
        banner += f"\n\t- {table}"

    banner += "\n\nThen run the script with the -y flag.\n"

    logger.warning(banner)
    logger.info("END")

    sys.exit()


def check_plw(fn: str) -> None:
    """Checks consistency of .PLW file path provided through CLI.

    Args:
        fn: provided CLI path to .PLW file
    """

    # Check file is of correct type
    if fn[-4:].upper() != '.PLW':
        logger.warning(f"Filename must be `.PLW`")
        sys.exit()

    path = Path(fn)

    # Check file exist
    if not path.is_file():
        logger.warning(f"Invalid path to .PLW file: `{path}`")
        sys.exit()


def check_plw_period(t: float) -> None:
    """Checks consistency of .PLW file data sampling period provided through
    CLI.

    .. tip::

        the data sampling period is most often ``1 s`` if the data was
        acquired through PicoLog Recorder, and ``3 s`` if the data was
        acquired through the ``pycotech`` package

    Args:
        t:  provided CLI .PLW file data sampling period. This should be the
            period between consecutive rows in the .PLW file.
    """

    # Check data sampling period is valid
    if t <= 0:
        logger.warning(f"Invalid .PLW data acquisition period: `{t}`")
        sys.exit()


def check_upload_period(t: float) -> None:
    """Checks consistency of data upload period provided through CLI.

    Args:
        t:  provided CLI data upload period. A period of ``0`` is allowed,
            so that it can be used for special cases.
    """

    # Check data upload period is valid
    if t < 0:
        logger.warning(f"Invalid data upload period: `{t}`")
        sys.exit()


def check_averaging_time(t: float) -> None:
    """Checks consistency of averaging time provided through CLI.

    Args:
        t:  provided CLI averaging time.
    """

    # Check averaging time is valid
    if t < 1:
        logger.warning(f"Invalid averaging time: `{t}`")
        sys.exit()

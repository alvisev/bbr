# Set user role to group role
SET_ROLE_RWX = 'SET ROLE strontium_lattice_rwx;'

# Reset user role
RESET_ROLE = 'RESET ROLE;'

# Initialize the table to hold temperature logs
CREATE_TABLE_BBR_LOGS = 'CREATE TABLE IF NOT EXISTS ' \
                        'bbr.logs' \
                        '(iso_timestamp TIMESTAMP PRIMARY KEY NOT NULL,' \
                        'unix_timestamp FLOAT,' \
                        'device varchar(255),' \
                        'channel INT,' \
                        'label varchar(255),' \
                        'calibrated_temperature FLOAT,' \
                        'calibration_applied FLOAT);'

CREATE_BBR_LOGS_HYPERTABLE = "SELECT * FROM " \
                             "create_hypertable(" \
                             "'bbr.logs', 'iso_timestamp', if_not_exists => TRUE" \
                             ");"

# Initialize the table to hold bbr corrections
CREATE_TABLE_BBR_CORRECTIONS = 'CREATE TABLE IF NOT EXISTS ' \
                               'bbr.corrections' \
                               '(iso_timestamp TIMESTAMP PRIMARY KEY NOT NULL,' \
                               'unix_timestamp FLOAT,' \
                               'clock varchar(255),' \
                               'channel varchar(255),' \
                               'max_temp FLOAT,' \
                               'min_temp FLOAT,' \
                               'max_grad FLOAT,' \
                               'shift FLOAT,' \
                               'shift_err FLOAT);'

CREATE_BBR_CORRECTIONS_HYPERTABLE = "SELECT * FROM " \
                             "create_hypertable(" \
                             "'bbr.corrections', 'iso_timestamp', if_not_exists => TRUE);"

# Disable USER triggers on bbr_logs
DISABLE_TRIGGER_BBR_LOGS = 'ALTER TABLE bbr.logs DISABLE ' \
                           'TRIGGER USER;'

# Disable USER triggers on bbr_corrections
DISABLE_TRIGGER_BBR_CORRECTIONS = 'ALTER TABLE ' \
                                  'bbr.corrections ' \
                                  'DISABLE TRIGGER USER;'

# Enable USER triggers on bbr_logs
ENABLE_TRIGGER_BBR_LOGS = 'ALTER TABLE bbr.logs ENABLE ' \
                          'TRIGGER USER;'

# Enable USER triggers on bbr_corrections
ENABLE_TRIGGER_BBR_CORRECTIONS = 'ALTER TABLE ' \
                                 'bbr.corrections ' \
                                 'ENABLE TRIGGER USER;'

# Delete entries from bbr_logs between time range
DELETE_TIMERANGE_FROM_BBR_LOGS = 'DELETE FROM ONLY ' \
                                 'bbr.logs WHERE ' \
                                 '(iso_timestamp BETWEEN %s AND %s);'

# Delete entries from bbr_corrections between time range
DELETE_TIMERANGE_FROM_BBR_CORRECTIONS = 'DELETE FROM ONLY ' \
                                        'bbr.corrections ' \
                                        'WHERE ' \
                                        '(iso_timestamp BETWEEN %s AND %s);'

# Select entries from bbr_corrections between time range
SELECT_TIMERANGE_FROM_BBR_CORRECTIONS = 'SELECT * FROM ' \
                                        'bbr.corrections ' \
                                        'WHERE iso_timestamp ' \
                                        'BETWEEN (%s) AND (%s);'

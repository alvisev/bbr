"""Bot logging readings from PicoLog Data Loggers (PT-104) to a PostgreSQL database.

This script continously logs readings from all four channels of each of the
five PT-104 we have in G04-L10 around Sr1. Readings are processed on the
fly, and uploaded to our PostgreSQL database on Vatican. Replaces the need for
using proprietary software (PLW Player) and cumbersome data analysis pipeline.

usage: pt104_streamer.py [-h] [-upt UPT] [-avgt AVGT] [-y]

optional arguments:
  -h, --help  show this help message and exit
  -upt UPT    period at which to upload data, in minutes. Set to 0 to upload at fastest available
              rate.
  -avgt AVGT  period over which to average temperatures for bbr shift calculation, in minutes.
  -y          PostgreSQL role has ownership.
"""

__author__ = "Alvise Vianello"
__status__ = "Prototype"
__version__ = "1.0.0"

import sys
import time
import jumbo
import logging
import pycotech
import argparse
import sql  # src/sql
import cli  # src/cli
import utils  # src/utils
import pandas as pd
from datetime import datetime
from collections import deque
from typing import List, Union

Bbr_logs_row = List[List[Union[float, int, str]]]


def process(queue: deque, clock_channels: cli.Channels,
            clock_channels_excluded: cli.Channels, num_batches: int,
            win_size: int, db: jumbo.database.Database, key: int) -> None:
    """Packs a list of channel-by-channel data-samples and associated
    metrics, into a dataframe which is then uploaded to the PostgreSQL
    database 'bbr_logs' table.

    Args:
        queue:          deque of batches. Each batch is a list of
                        single-channel entries from all available devices. Each
                        entry should be a list of
                            unix_timestamp
                            iso_timestamp
                            device
                            channel
                            label
                            calibrated_temperature
                            calibration_applied
        clock_channels: mapping between clock labels and associated data
                        acquisition channels.
        clock_channels_excluded: mapping between clock labels and associated
                        data acquisition channels which should not be
                        included in BBR correction calculation.
        num_batches:    number of new batches in the queue, i.e. number of
                        batches yet to be uploaded
        win_size:       size of the rolling average window used to smooth
                        the data-stream across each channel, in units of the
                        single channel acquisition period (usually 3s).
        db:             jumbo connection manager through which to execute the
                        transaction
        key:            key of the pool connection being used in the
                        transaction.
    """

    # TODO: if batch size becomes so big that maybe these
    #  methods are too slow, could rewrite functions to
    #  operate on queues directly instead of dataframes

    # Materialise queue: list(q) is a list of batches.
    # Each batch is a list of single channel entries. We
    # want to melt all entries into a stream
    # entries yet to be uploaded are at the end
    bbr_logs_columns = ['unix_timestamp', 'iso_timestamp', 'device', 'channel',
                        'label', 'calibrated_temperature',
                        'calibration_applied']

    cache_df = pd.DataFrame(
        [item for sublist in list(queue) for item in sublist],
        columns=bbr_logs_columns
    )

    # New entries are at the end of the queue
    num_new_entries = len(
        [item for sublist in list(queue)[-num_batches:] for item in sublist]
    )

    # Upload to database
    db.copy_df(df=cache_df.iloc[-num_new_entries:, :],
               db_table='bbr_logs', schema='strontium_lattice',
               replace=False, key=key)

    # If queue has full i.e. enough measurements for
    # window averaging:
    if len(queue) == queue.maxlen:

        # calc shifts, averaging over cache
        # output dataframe is a 'stream' dataframe
        df_bbr_cache = utils.build_bbr_corrections(
            df=cache_df, clock_channels=clock_channels,
            excluded_channels=clock_channels_excluded, win_size=win_size)

        # keep only new results (which should also be the
        # only ones with non NaN results)
        df_bbr = df_bbr_cache.iloc[-num_new_entries:, :]

        # Upload to database
        db.copy_df(df=df_bbr, db_table='bbr_corrections',
                   schema='strontium_lattice', replace=False, key=key)


if __name__ == "__main__":

    # Setup a basic logger
    logging.basicConfig(
        format='[%(asctime)s] %(levelname)s | %(message)s',
        datefmt='%D %H:%M:%S'
    )

    logger = logging.getLogger(__name__)
    logger.setLevel("INFO")

    print('\n')
    logger.info("Welcome to the PT-104 Data Streamer! \U0001F680 \n")
    logger.info("commit SHA: \t%s\n", cli.get_git_revision_hash())

    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-upt", help="period at which to upload data, in "
                                     "minutes. Set to 0 to upload at fastest "
                                     "available rate.",
                        required=False, type=int, default=1)
    parser.add_argument("-avgt", help="period over which to average "
                                      "temperatures for bbr shift "
                                      "calculation, in minutes.",
                        required=False, type=int, default=2)  # =120 @ 1s
    parser.add_argument("-y", help="PostgreSQL role has ownership.",
                        required=False, action='store_true')

    args = parser.parse_args()

    # Check data upload period is valid
    cli.check_upload_period(args.upt)
    num_batches = args.upt * 60 // 3  # pycotech collects 1 batch every 3s
    if num_batches == 0:
        num_batches = 1  # upload at every new batch, if period was set to 0mn

    # Check averaging time for BBR is valid
    cli.check_averaging_time(args.avgt)
    win_size = args.avgt * 60 // 3  # pycotech collects 1 batch every 3s

    # Force user to confirm he has ownership on database
    if not args.y:
        cli.force_rerun(check_tables=['strontium_lattice.bbr_logs',
                                      'strontium_lattice.bbr_corrections'])

    # load the config file with all pt104 parameters
    yaml_params = cli.load_yaml_config()
    ch_to_device = yaml_params.devices
    ch_to_calibrations = yaml_params.calibrations
    clk_to_channels = yaml_params.channels
    clk_to_channels_excluded = yaml_params.channels_excluded

    # Log out relevant CLI parameters values
    logger.info("- upload period:      %i mn\t(%i batches)",
                args.upt, num_batches)
    logger.info("- BBR averaging time: %i mn\n", args.avgt)

    # Sniff for devices to confirm that they are connected
    serials = pycotech.loggers.PT104().discover_devices()
    # Extract device serial numbers
    serials = serials.decode().split(',')
    serials = [serial.split(':')[1] for serial in serials]

    # if found any device...
    if serials != ['']:

        serials.sort()

        expected_serials = list(ch_to_device.values())
        expected_serials.sort()

        if serials != expected_serials:
            logger.info("\tSniffed devices don't match. Found %s. Expected %s",
                        serials, expected_serials)
            logger.info("END \U0001F440")
            sys.exit()

    else:
        logger.info("Could not find any connected devices...")
        logger.info("END \U0001F440")
        sys.exit()

    # MAIN
    
    # Assign an individual PT104 instance to each identifier.
    # If not you would have to call .connect() / .disconnect()
    # everytime you move to next device.
    # Run PT104().discover_devices() if need to sniff for devices
    devices = {serial: pycotech.loggers.PT104() for serial in
               ch_to_device.values()}

    # Reverse dictionary to map device serials to label family
    stumps = {serial: stump for stump, serial in ch_to_device.items()}

    logger.info("Starting database connection...")

    # Initialize database connection to upload temperature readings
    # jumbo.env file should be in root folder of this project
    database = jumbo.database.Database("../")

    # Create a connection pool
    with database.open() as pool:
        logger.info("\tclient:   \t%s", pool.config.DATABASE_USERNAME)
        logger.info("\tserver:   \t%s", pool.config.DATABASE_HOST)
        logger.info("\tdatabase: \t%s", pool.config.DATABASE_NAME)
        logger.info("\tschema:   \tstrontium_lattice")
        logger.info("\ttables:   \t[bbr_logs, bbr_corrections]")

        # Get individual connections from the pool
        with pool.connect(key=1):
            logger.info("\tDONE")

            logger.info('Creating table on database (if not present)...')
            # Switch to group role so that tables can then be accessed by all
            pool.send(sql.SET_GROUP_ROLE, key=1)
            # Initialize the table to hold temperature logs;
            pool.send(sql.CREATE_TABLE_BBR_LOGS, key=1)
            # Initialize the table to hold bbr analysis results;
            pool.send(sql.CREATE_TABLE_BBR_CORRECTIONS, key=1)
            # Reset to individual role
            pool.send(sql.RESET_ROLE, key=1)
            logger.info("\tDONE")

            # Disable all `USER` triggers (all triggers except foreign key)
            # to avoiding DDoS-ing the database when batch uploading
            logger.info('Disabling triggers on database tables...')
            pool.send(sql.DISABLE_TRIGGER_BBR_LOGS, key=1)
            pool.send(sql.DISABLE_TRIGGER_BBR_CORRECTIONS, key=1)
            logger.info("\tDONE")

            q = deque(maxlen=(win_size + num_batches - 1))
            idx = 0  # batch index
            try:

                # Connect to all devices
                logger.info("Connecting devices...")
                for name, device in devices.items():

                    device.connect(name)
                    logger.info("\t%s connected", name)

                    # Set all channels to correct settings
                    for ch_number in range(1, 5):
                        device.set_channel(
                            pycotech.loggers.channel_x(ch_number),
                            pycotech.loggers.DataTypes.PT100,
                            pycotech.loggers.Wires.WIRES_4
                        )

                logger.info("\tDONE\n")

                # Actual PT-104 Main Loop
                logger.info("Collecting data...\n")
                while True:

                    # Log data once collected enough samples
                    if idx >= num_batches:

                        # Process data queue
                        # Upload new calibrated temperature measurements
                        # Calculates and uploads associated bbr corrections
                        process(q, clock_channels=clk_to_channels,
                                clock_channels_excluded=clk_to_channels_excluded,
                                num_batches=idx, win_size=win_size, db=pool,
                                key=1)

                        # Reset counters
                        idx = 0

                    # Get measurements from all channels on all devices
                    # All 20 channels will be read virtually at once in 3
                    # seconds intervals (0.75 s per active channel)
                    batch = []
                    for name, device in devices.items():

                        for ch_number in range(1, 5):

                            # Microsleep to force a visible difference in
                            # successive timestamps
                            time.sleep(0.000001)

                            value = device.get_value(
                                pycotech.loggers.channel_x(ch_number)
                            )

                            timestamp_now = datetime.timestamp(
                                datetime.now()
                            )

                            if value:
                                timestamp_now_iso = datetime.fromtimestamp(
                                    timestamp_now).isoformat()

                                ch_name = stumps[name][0] + str(ch_number)  # eg C1

                                calibration = utils.get_calibration(
                                    value,
                                    ch_to_calibrations,
                                    ch_name)

                                calibrated_value = value + calibration

                                entry = [timestamp_now, timestamp_now_iso,
                                         name, ch_number, ch_name,
                                         calibrated_value, calibration]

                                # Log entry to measurement batch
                                batch.append(entry)

                    idx += 1  # +1 batch read
                    print("Samples collected since last upload: %5i" % idx,
                          end='\r')
                    q.append(batch)  # if full will auto-pop on the left

            except KeyboardInterrupt:
                print('\n')
                logger.warning("Received Keyboard Interrupt!\n")
            except Exception:
                print('\n')
                logger.exception("Exception raised!\n")

            finally:

                logger.info("Terminating program...")

                # Disconnect devices first
                logger.info("\tdisconnecting devices...")
                for name, device in devices.items():
                    device.disconnect()
                    logger.info("\t\t%s disconnected", name)

                # Make sure to save latest results
                logger.info("\tuploading cached data...")
                process(q, clock_channels=clk_to_channels,
                        clock_channels_excluded=clk_to_channels_excluded,
                        num_batches=idx,
                        win_size=win_size, db=pool, key=1)

                # Re-enable triggers
                logger.info('\trestoring triggers on database tables...')
                pool.send(sql.ENABLE_TRIGGER_BBR_LOGS, key=1)
                pool.send(sql.ENABLE_TRIGGER_BBR_CORRECTIONS, key=1)
                logger.info("\tDONE\n")

                logger.info("ALL DONE! \U0001F44D")


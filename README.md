# PLW Uploader

This script applies calibrations to temperature samples collected by the 
PicoLog PLW Recorder software - and saved in a .PLW file format. The 
processed data is uploaded to the PostgreSQL database, together with 
associated black-body radiation shift metrics.

## Requirements:

To run the script you need to have the following:


- The `.PLW` file you wish to process

- A valid `jumbo.env` database configuration file in the root directory of the 
  repo.


## Usage:

```bash
usage: plw_uploader.py [-h] -plw PLW [-t T] [-y] [--no_upload]

optional arguments:
  -h, --help   show this help message and exit
  -plw PLW     input .PLW file
  -t T         .PLW data acquisition period (s)
  -y           PostgreSQL role has ownership
  --no_upload  disable database interactions
```

## Minimal working example:

```bash
python plw_uploader.py -plw <path_to_plw_file.PLW>
```

Demo output:

![demo terminal output](./extras/demo_usage.PNG?raw=true)

# PT-104 Streamer

This script continuously collects data from connected PT-104 Data Loggers, 
applies calibrations, and streams results to the PostgreSQL database. 
Simultaneously, it also calculates associated BBR shift corrections and 
uploads them as well onto the database.

## Requirements:

To run the script you need to have the following:

- A valid `jumbo.env` database configuration file in the root directory of the repo.


## Usage:

```bash
usage: pt104_streamer.py [-h] [-upt UPT] [-avgt AVGT] [-y]

optional arguments:
  -h, --help  show this help message and exit
  -upt UPT    period at which to upload data, in minutes. Set to 0 to upload 
              at fastest available rate.
  -avgt AVGT  period over which to average temperatures for bbr shift 
              calculation, in minutes.
  -y          PostgreSQL role has ownership.
```

## Minimal working example:

```bash
python pt104_streamer.py
```


# BBR Downloader

This script downloads from the PostgreSQL database the BBR shifts associated 
with the temperature samples collected by the PicoLog PLW Recorder 
software in a given .PLW file.

The relevant data should have first been uploaded onto the database with 
`plw_uploader` or `pt104_streamer`.

Output files with BBR shifts data are generated for each individual clock. 

## Requirements:

To run the script you need to have the following:


- The `.PLW` file you wish download BBR shift data for

- A valid `jumbo.env` database configuration file in the root directory of the 
  repo.


## Usage:

```bash
usage: bbr_downloader.py [-h] -plw PLW [-t T] [-dir DIR]

optional arguments:
  -h, --help  show this help message and exit
  -plw PLW    reference .PLW file
  -t T        .PLW data acquisition period (s)
  -dir DIR    output files directory
```

## Minimal working example:

```bash
python bbr_downloader.py -plw <path_to_reference_plw_file.PLW>
```